package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"path"

	"codeberg.org/danjones000/slackmojidl/models"
)

func die(v any) {
	fmt.Fprintln(os.Stderr, v)
	os.Exit(1)
}

func main() {
	if len(os.Args) < 2 {
		die("Missing JSON file")
	}

	if len(os.Args) < 3 {
		die("Missing directory")
	}

	dir := os.Args[2]

	by, err := os.ReadFile(os.Args[1])
	if err != nil {
		die(err)
	}

	list := models.List{}
	err = json.Unmarshal(by, &list)
	if err != nil {
		die(err)
	}

	for _, emoji := range list.Emoji {
		dl(dir, emoji)
		saveJson(dir, emoji)
	}
}

func dl(dir string, emoji models.Emoji) {
	ext := path.Ext(emoji.Url)
	outPath := path.Join(dir, emoji.Name+"."+ext)
	out, err := os.Create(outPath)
	if err != nil {
		die(err)
	}
	defer out.Close()

	resp, err := http.Get(emoji.Url)
	if err != nil {
		die(err)
	}
	defer resp.Body.Close()

	_, err = io.Copy(out, resp.Body)
	if err != nil {
		die(err)
	}

	fmt.Println("Saved", emoji.Name, "to", outPath)

}

func saveJson(dir string, emoji models.Emoji) {
	outPath := path.Join(dir, emoji.Name+".json")
	out, err := os.Create(outPath)
	if err != nil {
		die(err)
	}
	defer out.Close()

	enc := json.NewEncoder(out)
	enc.SetIndent("", "    ")
	enc.Encode(emoji)

	fmt.Println("Saved data to ", outPath)
}
