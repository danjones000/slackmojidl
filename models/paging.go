package models

type Paging struct {
	Count uint32
	Total uint32
	Page  uint32
	Pages uint32
}
