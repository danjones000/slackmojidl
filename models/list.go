package models

type List struct {
	Ok            bool
	Emoji         []Emoji
	DisabledEmoji []any `json:"disabled_emoji"`
	Paging        Paging
}
