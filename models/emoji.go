package models

type Emoji struct {
	Name            string   `json:"name"`
	IsAlias         uint16   `json:"is_alias"`
	AliasFor        string   `json:"alias_for"`
	Url             string   `json:"url"`
	IsBad           bool     `json:"is_bad"`
	Synonyms        []string `json:"synonyms"`
	UserDisplayName string   `json:"user_display_name"`
}
